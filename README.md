# chrome-server


```mermaid
sequenceDiagram
    autonumber
    note over client: sends HTTP requests<br> to chrome-server

    client->>chrome-server: /eval expression=expr

    chrome-server-->> chrome: eval(expr)

    note over chrome: shows result of evaluation<br> on the web page or just calculate <br>and return the result

    chrome -->> chrome-server: result
    
    chrome-server -->> client: result


```

## Install

    go install .